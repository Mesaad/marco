/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradecalculator;


public class Deliverable {
    public String name;
    public int weight;

    public Deliverable(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Deliverable{" + "name=" + name + ", weight=" + weight + '}';
    }
    
    
}
