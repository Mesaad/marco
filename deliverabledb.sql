CREATE DATABASE deliverabledb;
use deliverabledb;
CREATE TABLE Deliverable (Name varchar(20), Weight int);
INSERT INTO Deliverable (Name, Weight) VALUES ("Assignment 1", 20);
INSERT INTO Deliverable (Name, Weight) VALUES ("Assignment 2", 20);
INSERT INTO Deliverable (Name, Weight) VALUES ("Assignment 3", 20);
INSERT INTO Deliverable (Name, Weight) VALUES ("Final Exam", 60);
